<?php


namespace Rockschtar\WordPress\Metabox\Tests;


use PHPUnit\Framework\TestCase;
use Rockschtar\WordPress\Metabox\Tests\Dummy\MetaboxDummy;
use function Brain\Monkey\setUp;
use function Brain\Monkey\tearDown;

class MetaboxTest extends TestCase {
    public function setUp(): void {
        parent::setUp();
        setUp();
    }

    public function tearDown(): void {
        tearDown();
        parent::tearDown();
    }

    public function testConstruct(): MetaboxDummy {

        $metabox = new MetaboxDummy();
        $this->assertTrue(has_action('save_post', [$metabox, '_save']));
        $this->assertTrue(has_action('admin_enqueue_scripts', [$metabox, '_adminEnqueueScripts']));
        $this->assertTrue(has_action('add_meta_boxes', [$metabox, 'addMetaBoxes']));

        return $metabox;

    }

    /**
     * @depends testConstruct
     * @param MetaboxDummy $metabox
     */
    public function testExecution(MetaboxDummy $metabox): void {
        do_action('admin_enqueue_scripts');
        do_action('save_post');
        do_action('add_meta_boxes');

        $this->assertSame(1, did_action('admin_enqueue_scripts'));
        $this->assertSame(1, did_action('save_post'));
        $this->assertSame(1, did_action('add_meta_boxes'));
    }


}
