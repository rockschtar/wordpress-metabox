<?php


namespace Rockschtar\WordPress\Metabox\Tests\Dummy;


use Rockschtar\WordPress\Metabox\Metabox;
use Rockschtar\WordPress\Metabox\Models\Context;
use Rockschtar\WordPress\Metabox\Models\MetaboxConfig;
use Rockschtar\WordPress\Metabox\Models\Priority;
use WP_Post;

class MetaboxDummy extends Metabox {

    /**
     * @return MetaboxConfig
     */
    public function config(): MetaboxConfig {
        $config = new MetaboxConfig();
        $config->setContext(Context::SIDE);
        $config->setId('unitest-metabox');
        $config->setPriority(Priority::LOW);
        $config->setTitle('Unittest Metabox');
        return $config;
    }

    /**
     * @param int $post_id
     * @param WP_Post $post
     * @param bool $update
     */
    public function save(int $post_id, WP_Post $post, bool $update): void {
        // TODO: Implement save() method.
    }

    /**
     * @param mixed $object
     * @param array $args
     */
    public function display($object, array $args = []): void {
        // TODO: Implement display() method.
    }

}