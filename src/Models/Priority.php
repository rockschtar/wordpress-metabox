<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Metabox\Models;

class Priority {

    public const HIGH = 'high';
    public const LOW = 'low';
}