<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Metabox\Models;

class Context {

    public const NORMAL = 'normal';
    public const SIDE = 'side';
    public const ADVANCED = 'advanced';

}