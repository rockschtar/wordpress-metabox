<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Metabox\Models;

class MetaboxConfig {

    private $add_meta_box_action = 'add_meta_boxes';
    /**
     * @var string
     */
    private $id;
    /**
     * @var string
     */
    private $title;
    /**
     * @var array
     */
    private $screens_or_posttypes = [];
    /**
     * @var string
     */
    private $context = Context::ADVANCED;
    /**
     * @var string
     */
    private $priority = 'default';
    /**
     * @var array
     */
    private $callback_args = [];
    /**
     * @var Columns|null
     */
    private $columns;

    /**
     * MetaboxConfig constructor.
     */
    public function __construct() {
        $this->columns = new Columns();
    }

    /**
     * @return string
     */
    public function getAddMetaBoxAction(): string {
        return $this->add_meta_box_action;
    }

    /**
     * @param string $add_meta_box_action
     * @return MetaboxConfig
     */
    public function setAddMetaBoxAction(string $add_meta_box_action): MetaboxConfig {
        $this->add_meta_box_action = $add_meta_box_action;
        return $this;
    }

    /**
     * @return null|Column
     */
    public function getColumns(): ?Columns {
        return $this->columns;
    }

    /**
     * @param Column $column
     * @return MetaboxConfig
     */
    public function addColumn(Column $column): MetaboxConfig {
        $this->columns->add($column);
        return $this;
    }

    /**
     * @return string
     */
    public function getId(): string {
        return $this->id;
    }

    /**
     * @param string $id
     * @return MetaboxConfig
     */
    public function setId(string $id): MetaboxConfig {
        $this->id = $id;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return MetaboxConfig
     */
    public function setTitle(string $title): MetaboxConfig {
        $this->title = $title;
        return $this;
    }

    /**
     * @return array
     */
    public function getScreensOrPosttypes(): array {
        return $this->screens_or_posttypes;
    }

    /**
     * @param array|null|string $screen
     * @return MetaboxConfig
     */
    public function addScreenOrPosttype($screen): MetaboxConfig {
        $this->screens_or_posttypes[] = $screen;
        return $this;
    }

    /**
     * @return string
     */
    public function getContext(): string {
        return $this->context;
    }

    /**
     * @param string $context
     * @return MetaboxConfig
     */
    public function setContext(string $context): MetaboxConfig {
        $this->context = $context;
        return $this;
    }

    /**
     * @return string
     */
    public function getPriority(): string {
        return $this->priority;
    }

    /**
     * @param string $priority
     * @return MetaboxConfig
     */
    public function setPriority(string $priority): MetaboxConfig {
        $this->priority = $priority;
        return $this;
    }

    /**
     * @return array|null
     */
    public function getCallbackArgs(): array {
        return $this->callback_args;
    }

    /**
     * @param array|null $callback_args
     * @return MetaboxConfig
     */
    public function setCallbackArgs(?array $callback_args): MetaboxConfig {
        $this->callback_args = $callback_args;
        return $this;
    }

}