<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Metabox\Models;

class Column {

    /**
     * @var string
     */
    private $key;

    /**
     * @var string
     */
    private $title;

    /**
     * @var int|null
     */
    private $offset;

    /**
     * @var Callable|null
     */
    private $callback;

    /**
     * Column constructor.
     * @param string $key
     * @param string $title
     * @param int|null $offset
     */
    public function __construct(string $key, string $title, callable $callback = null, int $offset = null) {
        $this->key = $key;
        $this->title = $title;
        $this->offset = $offset;
        $this->callback = $callback;
    }

    /**
     * @param string $key
     * @param string $title
     * @param callable|null $callback
     * @param int|null $offset
     * @return static;
     */
    public static function create(string $key, string $title, callable $callback = null, int $offset = null) {
        return new static($key, $title, $callback, $offset);
    }

    /**
     * @return int|null
     */
    public function getOffset(): ?int {
        return $this->offset;
    }

    /**
     * @param int|null $offset
     * @return Column
     */
    public function setOffset(?int $offset): Column {
        $this->offset = $offset;
        return $this;
    }

    /**
     * @return string
     */
    public function getKey(): string {
        return $this->key;
    }

    /**
     * @param string $key
     * @return Column
     */
    public function setKey(string $key): Column {
        $this->key = $key;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string {
        return $this->title;
    }

    /**
     * @param string $title
     * @return Column
     */
    public function setTitle(string $title): Column {
        $this->title = $title;
        return $this;
    }

    /**
     * @return Callable|null
     */
    public function getCallback(): ?Callable {
        return $this->callback;
    }

    /**
     * @param Callable $callback
     * @return Column
     */
    public function setCallback(Callable $callback): Column {
        $this->callback = $callback;
        return $this;
    }





}