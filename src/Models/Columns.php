<?php
/**
 * @author: StefanHelmer
 */

namespace Rockschtar\WordPress\Metabox\Models;


use ArrayIterator;
use IteratorAggregate;

class Columns implements IteratorAggregate {

    private $columns = [];

    public function add(Column $column): Columns {
        $this->columns[] = $column;
        return $this;
    }

    /**
     * @return Column[]|ArrayIterator
     */
    public function getIterator(): ArrayIterator {
        return new ArrayIterator($this->columns);
    }
}