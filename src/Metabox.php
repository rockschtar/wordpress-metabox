<?php
namespace Rockschtar\WordPress\Metabox;

use Rockschtar\WordPress\Metabox\Models\MetaboxConfig;
use WP_Post;

use function array_slice;
use function in_array;

abstract class Metabox
{

    private static $_instances = [];
    protected $save_post_priority = 10;

    /**
     * AbstractMetabox constructor.
     */
    final public function __construct()
    {
        add_action($this->config()->getAddMetaBoxAction(), array(&$this, 'addMetaBoxes'));
        add_action('save_post', array(&$this, '_save'), $this->save_post_priority, 3);
        add_action('admin_enqueue_scripts', array(&$this, '_adminEnqueueScripts'));

        $this->hooks();

        if ($this->config()->getColumns() !== null) {
            foreach ($this->config()->getScreensOrPosttypes() as $screen) {
                $this->addColumnHooks($screen);
            }
        }
    }

    public function hooks(): void
    {
    }

    /**
     * @return MetaboxConfig
     */
    abstract public function config(): MetaboxConfig;

    /**
     * @param int $post_id
     * @param WP_Post $post
     * @param bool $update
     */
    abstract public function save(int $post_id, WP_Post $post, bool $update): void;

    /**
     * @param mixed $object
     * @param array $args
     */
    abstract public function display($object, array $args = []): void;

    /**
     * @return static
     */
    final public static function &init()
    {
        $class = static::class;
        if (!isset(self::$_instances[$class])) {
            self::$_instances[$class] = new $class();
        }

        return self::$_instances[$class];
    }

    /**
     * @param $hook
     */
    final public function _adminEnqueueScripts($hook): void
    {
        global $screen;
        global $post_type;

        if (($hook === 'post.php' || $hook === 'post-new.php') && in_array(
                $post_type,
                $this->config()->getScreensOrPosttypes(),
                true
            )) {
            $this->enqueueScripts($hook);
        } elseif ($screen !== null && in_array($screen->id, $this->config()->getScreensOrPosttypes(), true)) {
            $this->enqueueScripts($hook);
        }
    }

    /**
     *
     */
    final public function addMetaBoxes(): void
    {
        add_meta_box(
            $this->config()->getId(),
            $this->config()->getTitle(),
            array(&$this, '_display'),
            $this->config()->getScreensOrPosttypes(),
            $this->config()->getContext(),
            $this->config()->getPriority(),
            $this->config()->getCallbackArgs()
        );
    }

    /**
     * @param array $columns
     * @return array
     */
    final public function _addCustomColumns(array $columns = []): array
    {
        if ($this->config()->getColumns() === null) {
            return $columns;
        }

        $meta_columns = $this->config()->getColumns();

        foreach ($meta_columns as $column) {
            if ($column !== null && $column->getOffset() !== null) {
                $columns =
                    array_slice($columns, 0, $column->getOffset(), true) + array(
                        $column->getKey() => $column->getTitle()
                    ) + array_slice($columns, $column->getOffset(), null, true);
            } else {
                $columns[$column->getKey()] = $column->getTitle();
            }
        }

        return $columns;
    }

    /**
     * @param int $postId
     * @param WP_Post $post
     * @param bool $update
     */
    final public function _save(int $postId, WP_Post $post, bool $update): void
    {
        if (!current_user_can('edit_post', $postId)) {
            return;
        }

        $metaboxId = sanitize_key($this->config()->getId());
        $nonce = filter_input(INPUT_POST, $metaboxId, FILTER_UNSAFE_RAW);

        if (!wp_verify_nonce($nonce, plugin_basename(__FILE__))) {
            return;
        }

        $this->save($postId, $post, $update);
        do_action('rswp_save_metabox', $postId, $post, $update);
        do_action('rswp_save_metabox_' . $post->post_type, $postId, $post, $update);

        if ($post->post_type !== $metaboxId) {
            do_action('rswp_save_metabox_' . $metaboxId, $postId, $post, $update);
        }
    }

    /**
     * @param mixed $object
     * @param array $args
     */
    final public function _display($object, array $args = []): void
    {
        $nonce_key = sanitize_key($this->config()->getId());
        wp_nonce_field(plugin_basename(__FILE__), $nonce_key);
        $this->display($object, $args);
    }

    /**
     * @param string $hook
     */
    public function enqueueScripts(string $hook): void
    {
        // feel free to override this
    }

    /**
     * @param string $column_key
     * @param int $post_id
     */
    final public function _columnContent(string $column_key, int $post_id): void
    {
        if ($this->config()->getColumns() === null) {
            return;
        }

        foreach ($this->config()->getColumns() as $column) {
            if ($column_key === $column->getKey() && $column->getCallback() !== null) {
                call_user_func($column->getCallback(), $post_id);
            }
        }
    }

    /**
     * @param string $screen
     */
    final public function addColumnHooks(string $screen): void
    {
        add_filter('manage_' . $screen . '_posts_columns', array(&$this, '_addCustomColumns'));
        add_action('manage_' . $screen . '_posts_custom_column', [&$this, '_columnContent'], 10, 2);
    }
}
